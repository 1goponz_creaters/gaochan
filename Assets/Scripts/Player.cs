using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class Player : Character, PlayerAct.I_3DActionActions
{
    PlayerAct._3DActionActions input;
    float moveX;
    float moveZ;
    [SerializeField]
    float cameraSpeed;
    [SerializeField]
    GameObject playerCamera;
    [SerializeField]
    float sneakCameraPosY;
    [SerializeField]
    GameObject menuUI;
    float rotateX;
    float rotateY;

    bool dash;
    bool sneak;

    Vector3 horizontal;

    override public void DoAwake()
    {
        input = new PlayerAct._3DActionActions(new PlayerAct());
        input.SetCallbacks(this);
        Cursor.lockState = CursorLockMode.Locked;
    }

    // インプットの有効・無効化
    void OnDestroy() => input.Disable();
    void OnEnable() => input.Enable();
    void OnDisable() => input.Disable();

    
    override public void DoUpdate()
    {
        RotateCameraAngle(rotateX, rotateY);
    }

    public override void DoFixedUpdate()
    {
        MoveHorizontal(moveX, moveZ);
    }

    public void MoveHorizontal(float X, float Z)
    {
        Vector3 cameraForward = Vector3.Scale(playerCamera.transform.forward, new Vector3(1, 0, 1)).normalized;
        Vector3 moveForward = Z * cameraForward * speed + X * playerCamera.transform.right * speed;
        if(sneak)
        {
            rb.velocity = moveForward / 2 + new Vector3(0, rb.velocity.y);
        }
        else if(dash)
        {
            rb.velocity = moveForward * 1.5f + new Vector3(0, rb.velocity.y);
        }
        else
        {
            rb.velocity = moveForward + new Vector3(0, rb.velocity.y);
        }

    }

    public void RotateCameraAngle(float X, float Y)
    {
        Vector3 angle = new Vector3(
            X,
            -Y,
            0
        );

        playerCamera.transform.eulerAngles += new Vector3(angle.y * cameraSpeed, angle.x * cameraSpeed);
        transform.eulerAngles += new Vector3(0, angle.x * cameraSpeed);
    }

    public void Warp(Vector3 afterPosition)
    {
        this.transform.position = afterPosition;
    }

    public void OnMove_Z(InputAction.CallbackContext context)
    {
        moveZ = context.ReadValue<float>();
    }

    public void OnMove_X(InputAction.CallbackContext context)
    {
        moveX = context.ReadValue<float>();
    }

    public void OnMouse_X(InputAction.CallbackContext context)
    {
        rotateX = context.ReadValue<float>();
    }

    public void OnMouse_Y(InputAction.CallbackContext context)
    {
        rotateY = context.ReadValue<float>();
    }

    public void OnDash(InputAction.CallbackContext context)
    {
        dash = context.ReadValueAsButton();
    }

    public void OnSneak(InputAction.CallbackContext context)
    {
        sneak = context.ReadValueAsButton();
        if(sneak)
        {
            playerCamera.transform.localPosition = new Vector3(0, sneakCameraPosY, 0);
        }
        else
        {
            playerCamera.transform.localPosition = new Vector3(0, 0, 0);
        }
    }

    public void OnNewaction(InputAction.CallbackContext context)
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void OnMenu(InputAction.CallbackContext context)
    {
        menuUI.SetActive(!menuUI.activeSelf);
    }

    public void OnAction(InputAction.CallbackContext context)
    {
        
    }
}