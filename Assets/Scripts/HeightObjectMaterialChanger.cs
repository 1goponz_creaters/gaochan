﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

public class HeightObjectMaterialChanger : MonoBehaviour
{
    public Material _material;           // 割り当てるマテリアル.
    private int i;

    // Use this for initialization
    void Start () {
        i = 0;
        foreach(GameObject obj in GameObject.FindGameObjectsWithTag("Map"))
        {
            Debug.Log(obj);
            obj.GetComponent<MeshRenderer>().material=_material;
        }
        
    }
}
