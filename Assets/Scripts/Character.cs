﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Character : MonoBehaviour
{
    protected Rigidbody rb;
    private Vector3 startPos;
    [SerializeField]
    protected float speed;
    [SerializeField]
    private bool fallReset;

    private void Awake()
    {
        startPos = transform.position;
        if(!gameObject.GetComponent<Rigidbody>())
            rb = gameObject.AddComponent<Rigidbody>();
        else
            rb = gameObject.GetComponent<Rigidbody>();
        DoAwake();
    }

    private void Update()
    {
        DoUpdate();
    }

    private void FixedUpdate()
    {
        DoFixedUpdate();
        if(transform.position.y <= -100)
        {
            if(fallReset)
            {
                transform.position = startPos;
            }
            else
            {
                Destroy(gameObject);
            }
        }
    }

    public void SetSpeed(float speed)
    {
        this.speed = speed;
    }

    public void SetGravity(float gravity)
    {
        Physics.gravity = new Vector3(0,-gravity,0);
    }

    abstract public void DoUpdate();
    abstract public void DoFixedUpdate();
    abstract public void DoAwake();
}
