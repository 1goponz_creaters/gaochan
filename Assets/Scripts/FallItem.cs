﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallItem : MonoBehaviour
{
    [SerializeField]
    private GameObject item;
    private GameObject instantiateItem;
    private BoxCollider boxCollider;
    [SerializeField]
    private bool autoDestroy;
    
    private void Awake()
    {
        boxCollider = GetComponent<BoxCollider>();
    }

    private void OnTriggerEnter(Collider other) {
        if(other.gameObject.tag == "Player")
        {
            Destroy(instantiateItem);
            instantiateItem = Instantiate(item, (other.transform.position + new Vector3(0, 5, 0) + other.transform.right*5), new Quaternion(0,0,0,0));
            Invoke("Destroy", 5f);
            
        }
    }

    private void Destroy()
    {
        if(autoDestroy)
        {
            Destroy(instantiateItem);
        }
    }
}
