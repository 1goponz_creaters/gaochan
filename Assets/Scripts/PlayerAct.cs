// GENERATED AUTOMATICALLY FROM 'Assets/PlayerAct.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @PlayerAct : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @PlayerAct()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""PlayerAct"",
    ""maps"": [
        {
            ""name"": ""3DAction"",
            ""id"": ""fe44edcd-ddde-4821-884b-6c79164cdb36"",
            ""actions"": [
                {
                    ""name"": ""Move_Z"",
                    ""type"": ""Value"",
                    ""id"": ""47f4809d-3217-4638-b35d-79be565f1560"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Move_X"",
                    ""type"": ""Button"",
                    ""id"": ""c8c8e0ba-4e36-46f2-95f7-143a1cac314f"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Mouse_X"",
                    ""type"": ""Value"",
                    ""id"": ""5e073fbd-68ef-43fd-a8db-0e0ef43d6804"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Mouse_Y"",
                    ""type"": ""Value"",
                    ""id"": ""76d92e33-6f78-4159-86c9-2053013fdd12"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Dash"",
                    ""type"": ""Button"",
                    ""id"": ""70fd4320-7fdf-4f65-8d13-08e6c14a17af"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Sneak"",
                    ""type"": ""Button"",
                    ""id"": ""44c90eb6-fa0b-4333-9af0-c43127b8e51a"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""New action"",
                    ""type"": ""Button"",
                    ""id"": ""914d0c73-d5d9-4626-b0f5-03958da5e125"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Menu"",
                    ""type"": ""Button"",
                    ""id"": ""dc71435d-36dd-4552-90c2-6f5b4b8a798d"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Action"",
                    ""type"": ""Button"",
                    ""id"": ""6ad49d5f-e41b-4060-8ac7-ed89adbab647"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""1D Axis"",
                    ""id"": ""9b95a6e8-9872-4c78-92bb-7606d977ac7b"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move_Z"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""c3e81aa8-3424-4239-a992-ab6f84614b63"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move_Z"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""91cf8eb8-b788-445f-9c48-538eec2410bc"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move_Z"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""1D Axis"",
                    ""id"": ""1e5198ec-a873-4905-9f3d-c0492e83444c"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move_X"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""55f24d03-28c1-4027-9dfe-a28373a98148"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move_X"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""27a8619a-594b-42fd-9db7-079a66ad53f9"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move_X"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""717b8600-7083-4848-ac6b-58355ff48819"",
                    ""path"": ""<Pointer>/delta/x"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Mouse_X"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""89e0efc6-793b-4c86-a683-c57aa11c9ef4"",
                    ""path"": ""<Pointer>/delta/y"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Mouse_Y"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c12292a4-8ce5-407b-9630-035dcc9dcac9"",
                    ""path"": ""<Keyboard>/shift"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Dash"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""83782448-2be2-4b72-963c-d4e4999b16dd"",
                    ""path"": ""<Keyboard>/ctrl"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Sneak"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ae2c77ef-f4ec-4904-906e-d130bb26b27f"",
                    ""path"": ""<Keyboard>/r"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""New action"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""9a8c8ee3-c7bf-4fec-b29e-603746e35645"",
                    ""path"": ""<Keyboard>/tab"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Menu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""48502afd-b96b-4d60-8be3-8dbe22d2fa8d"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Action"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""ActionObject"",
            ""id"": ""4c8f2c23-9940-476e-afb1-728e0528cc71"",
            ""actions"": [
                {
                    ""name"": ""Action"",
                    ""type"": ""Button"",
                    ""id"": ""fb1ca0ab-d619-428e-bf22-6cb532fe56f4"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""fbad9d18-5917-4e04-964e-df4560a43747"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Action"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // 3DAction
        m__3DAction = asset.FindActionMap("3DAction", throwIfNotFound: true);
        m__3DAction_Move_Z = m__3DAction.FindAction("Move_Z", throwIfNotFound: true);
        m__3DAction_Move_X = m__3DAction.FindAction("Move_X", throwIfNotFound: true);
        m__3DAction_Mouse_X = m__3DAction.FindAction("Mouse_X", throwIfNotFound: true);
        m__3DAction_Mouse_Y = m__3DAction.FindAction("Mouse_Y", throwIfNotFound: true);
        m__3DAction_Dash = m__3DAction.FindAction("Dash", throwIfNotFound: true);
        m__3DAction_Sneak = m__3DAction.FindAction("Sneak", throwIfNotFound: true);
        m__3DAction_Newaction = m__3DAction.FindAction("New action", throwIfNotFound: true);
        m__3DAction_Menu = m__3DAction.FindAction("Menu", throwIfNotFound: true);
        m__3DAction_Action = m__3DAction.FindAction("Action", throwIfNotFound: true);
        // ActionObject
        m_ActionObject = asset.FindActionMap("ActionObject", throwIfNotFound: true);
        m_ActionObject_Action = m_ActionObject.FindAction("Action", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // 3DAction
    private readonly InputActionMap m__3DAction;
    private I_3DActionActions m__3DActionActionsCallbackInterface;
    private readonly InputAction m__3DAction_Move_Z;
    private readonly InputAction m__3DAction_Move_X;
    private readonly InputAction m__3DAction_Mouse_X;
    private readonly InputAction m__3DAction_Mouse_Y;
    private readonly InputAction m__3DAction_Dash;
    private readonly InputAction m__3DAction_Sneak;
    private readonly InputAction m__3DAction_Newaction;
    private readonly InputAction m__3DAction_Menu;
    private readonly InputAction m__3DAction_Action;
    public struct _3DActionActions
    {
        private @PlayerAct m_Wrapper;
        public _3DActionActions(@PlayerAct wrapper) { m_Wrapper = wrapper; }
        public InputAction @Move_Z => m_Wrapper.m__3DAction_Move_Z;
        public InputAction @Move_X => m_Wrapper.m__3DAction_Move_X;
        public InputAction @Mouse_X => m_Wrapper.m__3DAction_Mouse_X;
        public InputAction @Mouse_Y => m_Wrapper.m__3DAction_Mouse_Y;
        public InputAction @Dash => m_Wrapper.m__3DAction_Dash;
        public InputAction @Sneak => m_Wrapper.m__3DAction_Sneak;
        public InputAction @Newaction => m_Wrapper.m__3DAction_Newaction;
        public InputAction @Menu => m_Wrapper.m__3DAction_Menu;
        public InputAction @Action => m_Wrapper.m__3DAction_Action;
        public InputActionMap Get() { return m_Wrapper.m__3DAction; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(_3DActionActions set) { return set.Get(); }
        public void SetCallbacks(I_3DActionActions instance)
        {
            if (m_Wrapper.m__3DActionActionsCallbackInterface != null)
            {
                @Move_Z.started -= m_Wrapper.m__3DActionActionsCallbackInterface.OnMove_Z;
                @Move_Z.performed -= m_Wrapper.m__3DActionActionsCallbackInterface.OnMove_Z;
                @Move_Z.canceled -= m_Wrapper.m__3DActionActionsCallbackInterface.OnMove_Z;
                @Move_X.started -= m_Wrapper.m__3DActionActionsCallbackInterface.OnMove_X;
                @Move_X.performed -= m_Wrapper.m__3DActionActionsCallbackInterface.OnMove_X;
                @Move_X.canceled -= m_Wrapper.m__3DActionActionsCallbackInterface.OnMove_X;
                @Mouse_X.started -= m_Wrapper.m__3DActionActionsCallbackInterface.OnMouse_X;
                @Mouse_X.performed -= m_Wrapper.m__3DActionActionsCallbackInterface.OnMouse_X;
                @Mouse_X.canceled -= m_Wrapper.m__3DActionActionsCallbackInterface.OnMouse_X;
                @Mouse_Y.started -= m_Wrapper.m__3DActionActionsCallbackInterface.OnMouse_Y;
                @Mouse_Y.performed -= m_Wrapper.m__3DActionActionsCallbackInterface.OnMouse_Y;
                @Mouse_Y.canceled -= m_Wrapper.m__3DActionActionsCallbackInterface.OnMouse_Y;
                @Dash.started -= m_Wrapper.m__3DActionActionsCallbackInterface.OnDash;
                @Dash.performed -= m_Wrapper.m__3DActionActionsCallbackInterface.OnDash;
                @Dash.canceled -= m_Wrapper.m__3DActionActionsCallbackInterface.OnDash;
                @Sneak.started -= m_Wrapper.m__3DActionActionsCallbackInterface.OnSneak;
                @Sneak.performed -= m_Wrapper.m__3DActionActionsCallbackInterface.OnSneak;
                @Sneak.canceled -= m_Wrapper.m__3DActionActionsCallbackInterface.OnSneak;
                @Newaction.started -= m_Wrapper.m__3DActionActionsCallbackInterface.OnNewaction;
                @Newaction.performed -= m_Wrapper.m__3DActionActionsCallbackInterface.OnNewaction;
                @Newaction.canceled -= m_Wrapper.m__3DActionActionsCallbackInterface.OnNewaction;
                @Menu.started -= m_Wrapper.m__3DActionActionsCallbackInterface.OnMenu;
                @Menu.performed -= m_Wrapper.m__3DActionActionsCallbackInterface.OnMenu;
                @Menu.canceled -= m_Wrapper.m__3DActionActionsCallbackInterface.OnMenu;
                @Action.started -= m_Wrapper.m__3DActionActionsCallbackInterface.OnAction;
                @Action.performed -= m_Wrapper.m__3DActionActionsCallbackInterface.OnAction;
                @Action.canceled -= m_Wrapper.m__3DActionActionsCallbackInterface.OnAction;
            }
            m_Wrapper.m__3DActionActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Move_Z.started += instance.OnMove_Z;
                @Move_Z.performed += instance.OnMove_Z;
                @Move_Z.canceled += instance.OnMove_Z;
                @Move_X.started += instance.OnMove_X;
                @Move_X.performed += instance.OnMove_X;
                @Move_X.canceled += instance.OnMove_X;
                @Mouse_X.started += instance.OnMouse_X;
                @Mouse_X.performed += instance.OnMouse_X;
                @Mouse_X.canceled += instance.OnMouse_X;
                @Mouse_Y.started += instance.OnMouse_Y;
                @Mouse_Y.performed += instance.OnMouse_Y;
                @Mouse_Y.canceled += instance.OnMouse_Y;
                @Dash.started += instance.OnDash;
                @Dash.performed += instance.OnDash;
                @Dash.canceled += instance.OnDash;
                @Sneak.started += instance.OnSneak;
                @Sneak.performed += instance.OnSneak;
                @Sneak.canceled += instance.OnSneak;
                @Newaction.started += instance.OnNewaction;
                @Newaction.performed += instance.OnNewaction;
                @Newaction.canceled += instance.OnNewaction;
                @Menu.started += instance.OnMenu;
                @Menu.performed += instance.OnMenu;
                @Menu.canceled += instance.OnMenu;
                @Action.started += instance.OnAction;
                @Action.performed += instance.OnAction;
                @Action.canceled += instance.OnAction;
            }
        }
    }
    public _3DActionActions @_3DAction => new _3DActionActions(this);

    // ActionObject
    private readonly InputActionMap m_ActionObject;
    private IActionObjectActions m_ActionObjectActionsCallbackInterface;
    private readonly InputAction m_ActionObject_Action;
    public struct ActionObjectActions
    {
        private @PlayerAct m_Wrapper;
        public ActionObjectActions(@PlayerAct wrapper) { m_Wrapper = wrapper; }
        public InputAction @Action => m_Wrapper.m_ActionObject_Action;
        public InputActionMap Get() { return m_Wrapper.m_ActionObject; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(ActionObjectActions set) { return set.Get(); }
        public void SetCallbacks(IActionObjectActions instance)
        {
            if (m_Wrapper.m_ActionObjectActionsCallbackInterface != null)
            {
                @Action.started -= m_Wrapper.m_ActionObjectActionsCallbackInterface.OnAction;
                @Action.performed -= m_Wrapper.m_ActionObjectActionsCallbackInterface.OnAction;
                @Action.canceled -= m_Wrapper.m_ActionObjectActionsCallbackInterface.OnAction;
            }
            m_Wrapper.m_ActionObjectActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Action.started += instance.OnAction;
                @Action.performed += instance.OnAction;
                @Action.canceled += instance.OnAction;
            }
        }
    }
    public ActionObjectActions @ActionObject => new ActionObjectActions(this);
    public interface I_3DActionActions
    {
        void OnMove_Z(InputAction.CallbackContext context);
        void OnMove_X(InputAction.CallbackContext context);
        void OnMouse_X(InputAction.CallbackContext context);
        void OnMouse_Y(InputAction.CallbackContext context);
        void OnDash(InputAction.CallbackContext context);
        void OnSneak(InputAction.CallbackContext context);
        void OnNewaction(InputAction.CallbackContext context);
        void OnMenu(InputAction.CallbackContext context);
        void OnAction(InputAction.CallbackContext context);
    }
    public interface IActionObjectActions
    {
        void OnAction(InputAction.CallbackContext context);
    }
}
