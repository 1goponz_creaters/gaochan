﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class GaoChang : Character
{
    [SerializeField]
    private float distanceLimit;

    private GameObject player;
    private Vector3 _forward;


    public override void DoAwake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        _forward = Vector3.forward;
    }

    public override void DoFixedUpdate()
    {
    }

    public override void DoUpdate()
    {
        float distanceToPlayer = Vector3.Distance(player.transform.position, transform.position);
        Debug.Log("プレイヤーまでの距離" + distanceToPlayer);
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            SearchPlayer();
        }
    }


    void SearchPlayer()
    {
        Debug.Log("プレイヤー探す");
        var aim = (player.transform.position - transform.position);
        var distance = aim.magnitude;
        var direction = aim / distance;
        //Rayの作成　　　　　　　↓Rayを飛ばす原点　　　↓Rayを飛ばす方向
        Ray ray = new Ray(transform.position, direction);
        //Rayが当たったオブジェクトの情報を入れる箱
        RaycastHit hit;
        //Rayの飛ばせる距離
        //Rayの可視化    ↓Rayの原点　　　　↓Rayの方向　　　　　　　　　↓Rayの色
        Debug.DrawLine(ray.origin, ray.origin + ray.direction * distance, Color.red);
        //もしRayにオブジェクトが衝突したら
        //                  ↓Ray  ↓Rayが当たったオブジェクト ↓距離
        if (Physics.Raycast(ray, out hit, distance))
        {
            //Rayが当たったオブジェクトのtagがPlayerだったら
            if (hit.collider.tag == "Player")
            {
                Debug.Log("RayがPlayerに当たった");
                direction.y = 0;
                var lookRotation = Quaternion.FromToRotation(-_forward, direction);
                transform.rotation = Quaternion.Lerp(transform.rotation, lookRotation, 0.085f);
                Debug.Log(transform.forward);
                rb.velocity = -transform.forward * speed;

            }

        }
        Debug.Log("プレイヤー探す終わり");
    }
}
