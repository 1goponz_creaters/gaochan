﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Climb : ActionArea
{
    public override void Action(Player player)
    {
        player.Warp(transform.GetChild(0).position);
    }
}
