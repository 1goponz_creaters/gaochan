﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

abstract public class ActionArea : MonoBehaviour, PlayerAct.IActionObjectActions
{
    protected PlayerAct.ActionObjectActions action;
    public bool isInArea;
    public Player player;

    public void Awake()
    {
        action = new PlayerAct.ActionObjectActions(new PlayerAct());
        action.SetCallbacks(this);
    }

    // インプットの有効・無効化
    void OnDestroy() => action.Disable();
    void OnEnable() => action.Enable();

    void OnDisable() => action.Disable();

    private void OnTriggerEnter(Collider other) {
        if(other.gameObject.tag == "Player")
        {
            isInArea = true;
            player = other.gameObject.GetComponent<Player>();
        }
    }

    private void OnTriggerExit(Collider other) {
        if(other.gameObject.tag == "Player")
        {
            isInArea = false;
            player = null;
        }
    }

    abstract public void Action(Player player);

    public void OnAction(InputAction.CallbackContext context)
    {
        if(isInArea)
        {
            if(player)
            {
                Debug.Log(player);
                Action(player);
            }
        }
    }
}
